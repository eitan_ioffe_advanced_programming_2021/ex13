#pragma once
#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>

using std::string;

class Message
{
public:
	Message(SOCKET sck, string name, string data);
	~Message();

	// getters
	string getName() const;
	string getData() const;
	SOCKET getSocket() const;

	// setters
	void setName(const string name);
	void setData(const string data);
	void setSocket(const SOCKET sck);

	int getCode() const; // returns code - 3 first numbers

private:
	string _name;
	string _data;
	SOCKET _socket;
};
