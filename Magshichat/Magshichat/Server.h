#pragma once
#include <iostream>
#include <utility>
#include <fstream>
#include <thread>
#include <queue>
#include <mutex>
#include <set>
#include "Message.h"
#include "Helper.h"

using std::cout;
using std::endl;
using std::string;

#define BYTE_SIZE 256
#define CONFIG_FILE "config.txt"

class Server
{
public:
	Server();
	~Server();

	void serve();

private:
	int getPortFromFile(); // getting only the port from config file - ip is always local

	void accept(); // accepting each client and creating a new thread for him
	void clientHandler(SOCKET clientSocket); // getting msgs from client and pushing them to queue
	void messageHandler(); // handling each msg in the queue

	void logIn(Message msg); // inserting new name to set
	void sendMsg(Message msg); // saving conversation in file and than sending it to client

	void updateMsgQueue(SOCKET sck, string name, string data); // adding new msg to queue
	string connectedUsers(); // returning a string of all connected users

	SOCKET _serverSocket;


	std::mutex _messageMtx;
	std::queue <Message> _messages;
	std::set <string> _users;
	std::condition_variable cv;
	bool ready;
};
