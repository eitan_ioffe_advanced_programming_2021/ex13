#include "Message.h"

Message::Message(SOCKET sck, string name, string data)
{
	_socket = sck;
	_name = name;
	_data = data;
}

Message::~Message()
{
}

string Message::getName() const
{
	return _name;
}

string Message::getData() const
{
	return _data;
}

SOCKET Message::getSocket() const
{
	return _socket;
}

void Message::setName(const string name)
{
	_name = name;
}

void Message::setData(const string data)
{
	_data = data;
}

void Message::setSocket(const SOCKET sck)
{
	_socket = sck;
}

int Message::getCode() const
{
	return std::stoi(_data.substr(0, 3));
}
