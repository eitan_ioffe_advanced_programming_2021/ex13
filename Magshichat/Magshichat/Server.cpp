#include "Server.h"
#include <sstream>

Server::Server()
{
	ready = false;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve()
{
	int port = getPortFromFile();
	struct sockaddr_in sa = { 0 };

	cout << "Starting..." << endl;
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	cout << "binded" << endl;

	// Start listening for incoming requests of clients
	cout << "listening..." << endl;
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	cout << "accepting client..." << endl;
	std::thread t(&Server::messageHandler, this);
	t.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and creates a thread for each client
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

int Server::getPortFromFile()
{
	string buffer = "";
	std::ifstream myfile(CONFIG_FILE);
	if (myfile.is_open())
	{
		std::getline(myfile, buffer);
		std::getline(myfile, buffer); // getting second line
		return std::stoi(buffer.substr(5)); // returning the port from line
	}
	else throw std::exception("Could not open configure file");

	return 0;
}

void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the thread that is created for each client
	std::thread clientThread(&Server::clientHandler, this, client_socket);
	clientThread.detach();
}

void Server::clientHandler(SOCKET clientSocket)
{
	string msg = "", name = "";
	try
	{
		msg = Helper::getStringPartFromSocket(clientSocket, BYTE_SIZE); // getting login msg
		name = Helper::getUserName(msg); // getting name of user from his login msg
		std::cout << name << " has logged in (" << clientSocket << ")" << std::endl;

		while (true)
		{
			// adding to the queue the client's msg (including his details)
			updateMsgQueue(clientSocket, name, msg);
			
			msg = Helper::getStringPartFromSocket(clientSocket, BYTE_SIZE); // getting next msg
		}
	}
	catch (const std::exception& e)
	{
		msg = "208" + Helper::getPaddedNumber(name.length(), 2) + name; // creating a new exit msg
		updateMsgQueue(clientSocket, name, msg); // adding exit msg to queue

		cout << "User " << name << " has disconnected (" << clientSocket << ")" << endl;
	}
}

void Server::messageHandler()
{
	Message msg(0, "", "");

	while (true)
	{
		// waiting until getting notified about new msgs in queue
		std::unique_lock<std::mutex> lckMsg(_messageMtx);
		if (!ready) cv.wait(lckMsg);
		ready = false;
		lckMsg.unlock();

		lckMsg.lock(); // locking msg because we check if it is empty
		while (!_messages.empty())
		{
			msg = _messages.front();
			_messages.pop();
			lckMsg.unlock(); // unlocking msg - not using right now
			
			switch (msg.getCode()) // handling each type of msg
			{
			case MT_CLIENT_LOG_IN:
				logIn(msg);
				break;
			case MT_CLIENT_UPDATE:
				sendMsg(msg);
				break;
			case MT_CLIENT_EXIT:
				_users.erase(msg.getName()); // removing user from set
				closesocket(msg.getSocket()); // closing his socket
				break;
			}

			lckMsg.lock(); // locking msg back
		}

		lckMsg.unlock();
	}
}

void Server::logIn(Message msg)
{
	string name = msg.getName();
	_users.insert(name);	// note: if an existing name is entered, the server will treat the client
							// as the same user

	Helper::send_update_message_to_client(msg.getSocket(), "", "", connectedUsers());
}

void Server::sendMsg(Message msg)
{
	string dstUserName = Helper::getUserName(msg.getData()), content = Helper::getMsgContent(msg.getData()), nameOfFile = "";

	// creating the file name (alphabetical order)
	if (msg.getName() > dstUserName)
		nameOfFile = dstUserName + "&" + msg.getName() + ".txt";
	else
		nameOfFile = msg.getName() + "&" + dstUserName + ".txt";

	if (content != "") { // updating file in case client sent a new msg to another client
		std::fstream file;
		file.open(nameOfFile, std::fstream::app); // opening file for writing (without overwriting)
		file << "&MAGSH_MESSAGE&&Author&" + msg.getName() + "&DATA&" + content;
		file.close();
	}
	
	// getting the full conversation from the file of 2 clients
	content = "";
	string buffer = "";
	std::ifstream myfile(nameOfFile);
	if (myfile.is_open()) // if the could open the file
	{
		while (std::getline(myfile, buffer)) {
			content += buffer;
		}
		myfile.close();
	}
	
	// sending update msg with the content (if exists)
	Helper::send_update_message_to_client(msg.getSocket(), content, dstUserName, connectedUsers());
}

void Server::updateMsgQueue(SOCKET sck, string name, string data)
{
	// pushing msg from user to queue and
	// notifying other threads that there are new messages to handle
	std::unique_lock<std::mutex> lck(_messageMtx);
	Message m(sck, name, data);
	_messages.push(m);
	ready = true;
	lck.unlock();
	cv.notify_all();
}

string Server::connectedUsers()
{
	// converting set to a string with & between each one
	std::ostringstream stream;
	std::copy(_users.begin(), _users.end(), std::ostream_iterator<std::string>(stream, "&"));
	std::string connectedUsers = stream.str();
	if (connectedUsers != "") // removing '&' from end of string
		connectedUsers = connectedUsers.substr(0, connectedUsers.size() - 1);

	return connectedUsers;
}
